import unittest
from calculator import Calculator

class TestMyCalculator(unittest.TestCase):
    
    def setUp(self):
        self.calc = Calculator()
    
    
    def test_initial_value(self):
        self.assertEqual(0,self.calc.value)

    def test_add_method(self):
        self.calc.add(5,2)
        self.assertEqual(7,self.calc.value)
    
    def test_substract_method(self):
        self.calc.substract(5,2)
        self.assertEqual(3,self.calc.value)
    
    def test_multiply_method(self):
        self.calc.multiply(5,2)
        self.assertEqual(10,self.calc.value)
        
    def test_divide_method(self):
        self.calc.divide(5,2)
        self.assertEqual(2.5,self.calc.value)